package com.postsuperman.superback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperBackApplication.class, args);
    }

}
