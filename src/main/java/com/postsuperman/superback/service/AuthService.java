package com.postsuperman.superback.service;

import com.postsuperman.superback.common.GenericException;

import java.util.Map;

public interface AuthService {

    Map login(String username, String password) throws GenericException;

    String refreshToken(String oldToken);
}
