package com.postsuperman.superback.service;

import com.postsuperman.superback.common.PageModel;
import com.postsuperman.superback.entity.BO.HttpShareBO;
import com.postsuperman.superback.entity.PO.HttpSharePO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.entity.VO.HttpShareVO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface HttpShareService {
    HttpShareVO getHttpShare(String id);

    HttpSharePO addHttpShare(HttpShareBO httpShareBO);

    PageModel getHttpShareListByPage(String shareName, Integer pageNo, Integer pageSize);

    List getHttpShareList();


    void deleteHttpShare(String id);

    void updateHttpShare(HttpShareBO httpShareBO);

    HttpTaskPO getHttpTaskDetail(String uuid,String taskId);
}