package com.postsuperman.superback.service;



import com.postsuperman.superback.entity.BO.UserBO;
import com.postsuperman.superback.entity.VO.UserAddVO;
import com.postsuperman.superback.entity.VO.UserVO;

import java.util.List;

public interface UserService {

    UserBO getUserById(String id);

    UserBO getUserByUsername(String username);

    UserBO getUserByPhone(String phone);

    List<UserBO> getAllUsers();

    List<UserBO> getUsersByRole(String role);

    String saveUser(UserAddVO userAddVO);

    String registerUser(UserAddVO userAddVO);
//
    void editUser(String id, UserAddVO userAddVO);

    void deleteUser(String id);

    Boolean isUserExist(String id);

    public UserVO userBO2UserVO(UserBO userBO);

}
