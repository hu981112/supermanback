package com.postsuperman.superback.service;

import com.postsuperman.superback.entity.PO.EnvVarPO;

import java.util.List;

public interface EnvVarService  {
    void addEnvVar(EnvVarPO envVarPO);

    void updateEnvVar(EnvVarPO envVarPO);

    void deleteEnvVar(String envVarName);

    EnvVarPO getEnvVar(String envVarId);

    List<EnvVarPO> getEnvVars();

    List<EnvVarPO> getEnvVarByUser(String userId);
}
