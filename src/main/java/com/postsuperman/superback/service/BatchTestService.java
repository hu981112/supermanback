package com.postsuperman.superback.service;

import com.postsuperman.superback.entity.PO.BatchTestPO;

import java.util.List;

public interface BatchTestService {
    void addBatchTest(BatchTestPO batchTestPO);

    void deleteBatchTest(String id);

    List<BatchTestPO> getBatchTestList();

    BatchTestPO doBatchTest(String id, Boolean catchable, Integer times);

    BatchTestPO doBatchTest(BatchTestPO batchTestPO);
}
