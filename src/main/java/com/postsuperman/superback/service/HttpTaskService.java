package com.postsuperman.superback.service;

import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpTaskBO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface HttpTaskService {

    void addTask(HttpTaskBO httpTaskBO);

    void updateTask(HttpTaskPO httpTaskPO);

    void deleteTask(String id);

    HttpTaskPO getTask(String id);

    List getTaskList(HttpTaskPO httpTaskPO);

    Integer getTaskCount();

    List<HttpTaskPO> getTaskVOByUser(String userId);

    HttpTaskBO RunTask(HttpReqBO httpReqBO);

    byte[] downloadFile(String id);

    void uploadFile(MultipartFile file);
}