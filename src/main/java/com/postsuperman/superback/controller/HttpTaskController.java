package com.postsuperman.superback.controller;

import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpTaskBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.service.HttpTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


@RestController
@RequestMapping("/api/httptask")
public class HttpTaskController {
    
    @Autowired
    private HttpTaskService httpTaskService;

    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse list(@RequestBody @Valid HttpTaskPO httpTaskPO) {
        httpTaskPO.setUserid(SecurityContextHolder.getContext().getAuthentication().getName());
        return GenericResponse.success(httpTaskService.getTaskList(httpTaskPO));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse get(@PathVariable("id") @NotNull String id) {
        return  GenericResponse.success( httpTaskService.getTask(id));
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse create(@RequestBody @Valid HttpTaskBO httpTaskBO)  {
        httpTaskService.addTask(httpTaskBO);
        return GenericResponse.success();
    }

    @PostMapping("/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse update(@RequestBody @Valid HttpTaskPO httpTaskPO) throws IOException {
        httpTaskService.updateTask(httpTaskPO);
        return GenericResponse.success();
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse delete(@PathVariable @Valid @NotNull String id) {
        httpTaskService.deleteTask(id);
        return GenericResponse.success();
    }

    @PostMapping("/run")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse run(@RequestBody @Valid HttpReqBO httpReqBO) throws IOException {
        return GenericResponse.success(httpTaskService.RunTask(httpReqBO));
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse getAll() {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        return GenericResponse.success(httpTaskService.getTaskVOByUser(userid));
    }

    @GetMapping("/download/{id}")
    public ResponseEntity<Object> image(@PathVariable("id")String id) {
        byte[] bytes = httpTaskService.downloadFile(id);
        if (bytes == null) {
            return ResponseEntity.notFound().build();
        }
        String filename=id+".json";
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType =MediaType.parseMediaType("text/xml;charset=utf-8");
        if (MediaTypeFactory.getMediaType(filename).isPresent()) {
            mediaType = MediaTypeFactory.getMediaType(filename).get();
        }
        headers.add( "Cache-Control","no-cache,no-store,must-revalidate" );
        headers.add( "Pragma","no-cache" );
        headers.add( "Expires","0" );
        headers.setContentDispositionFormData("attachment", new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(bytes));
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(mediaType)
                .body(resource);
    }

    @PostMapping("/upload")
    public @ResponseBody GenericResponse upload(@RequestParam("file") MultipartFile file){
        httpTaskService.uploadFile(file);
        return GenericResponse.success();
    }



}
