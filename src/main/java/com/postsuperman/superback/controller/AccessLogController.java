package com.postsuperman.superback.controller;


import com.postsuperman.superback.common.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/logs/access")
public class AccessLogController {

    private final MongoTemplate mongoTemplate;

    public AccessLogController(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @GetMapping("/")
    public GenericResponse getAccessLogs(@NotNull Integer pageNo, @NotNull Integer pageSize) {
        Query query = new Query();
        Long count = mongoTemplate.count(query, AccessLog.class);
        query.skip((pageNo - 1) * pageSize);
        query.limit(pageSize);
        List datas = mongoTemplate.find(query,AccessLog.class);
        PageModel pageModel = PageModel.builder()
                .datas(datas)
                .pageNo(pageNo)
                .pageSize(pageSize)
                .rowCounts(count)
                .build();
        return GenericResponse.success(pageModel);
    }

    @DeleteMapping("/{id}")
    public GenericResponse delete(@PathVariable String id) {
        if(mongoTemplate.findById(id, AccessLog.class) == null)
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"日志不存在！");
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query,AccessLog.class);
        return GenericResponse.success();
    }
}
