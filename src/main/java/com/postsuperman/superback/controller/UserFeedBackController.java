package com.postsuperman.superback.controller;

import com.postsuperman.superback.common.*;
import com.postsuperman.superback.entity.PO.UserFeedBackPO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/feedback")
public class UserFeedBackController {
    private final MongoTemplate mongoTemplate;

    public UserFeedBackController(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    @PostMapping("/create")
    public GenericResponse addUserFeedBack(@RequestBody UserFeedBackPO userFeedBackPO) {
        userFeedBackPO.setCreateTime(new Date());
        userFeedBackPO.setUserid(SecurityContextHolder.getContext().getAuthentication().getName());
        mongoTemplate.save(userFeedBackPO);
        return GenericResponse.success();
    }

    @GetMapping("/")
    public GenericResponse getAccessLogs(String type,@NotNull Integer pageNo, @NotNull Integer pageSize) {
        Query query = new Query();
        if(StringUtils.isNotBlank(type))
            query.addCriteria(Criteria.where("type").is(type));
        Long count = mongoTemplate.count(query, UserFeedBackPO.class);
        query.skip((pageNo - 1) * pageSize);
        query.limit(pageSize);
        List datas = mongoTemplate.find(query,UserFeedBackPO.class);
        PageModel pageModel = PageModel.builder()
                .datas(datas)
                .pageNo(pageNo)
                .pageSize(pageSize)
                .rowCounts(count)
                .build();
        return GenericResponse.success(pageModel);
    }

    @DeleteMapping("/{id}")
    public GenericResponse delete(@PathVariable String id) {
        if(mongoTemplate.findById(id, UserFeedBackPO.class) == null)
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"反馈不存在！");
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query,UserFeedBackPO.class);
        return GenericResponse.success();
    }
}
