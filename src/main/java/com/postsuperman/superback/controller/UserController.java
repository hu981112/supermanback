package com.postsuperman.superback.controller;


import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.entity.BO.UserBO;
import com.postsuperman.superback.entity.VO.UserAddVO;
import com.postsuperman.superback.entity.VO.UserVO;
import com.postsuperman.superback.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")

public class UserController {

    private UserService userService;


    @Resource(name = "userServiceImpl")
    private UserDetailsService userDetailsService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public GenericResponse getAllUsers() {
        List<UserBO> userBOS = userService.getAllUsers();
        if (userBOS.size() == 0) {
            return GenericResponse.success(userBOS);
        }
        List<UserVO> res = userBOS.stream().map(u -> userService.userBO2UserVO(u)).collect(Collectors.toList());
        return GenericResponse.success(res);
    }
    @PostMapping("/register")
    public GenericResponse registerUsers(@RequestBody @Validated UserAddVO userAddVO) {
        String id = userService.saveUser(userAddVO);
        return GenericResponse.success(userService.userBO2UserVO(userService.getUserById(id)));
    }

    @GetMapping("/{id}")
    public GenericResponse getUserById(@PathVariable String id) {
        UserBO userBO = userService.getUserById(id);
        if (userBO == null) {
            return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "用户不存在");
        }
        UserVO userVO = userService.userBO2UserVO(userBO);
        return GenericResponse.success(userVO);
    }

    @GetMapping("/current")
    public GenericResponse getCurrentUser() {
        UserBO userBO = (UserBO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserVO userVO = userService.userBO2UserVO(userBO);
        return GenericResponse.success(userVO);
    }


    @GetMapping("/info")
    public GenericResponse getUserBO() {
        UserBO userBO = (UserBO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        UserVO userVO = userService.userBO2UserVO(userBO);
        return GenericResponse.success(userBO);
    }


    @PostMapping("/")
    public GenericResponse addUser(@RequestBody @Validated UserAddVO userAddVO) {
        String id = userService.saveUser(userAddVO);
        return GenericResponse.success(userService.userBO2UserVO(userService.getUserById(id)));
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasAuthority('editUser')")
    public GenericResponse updateUser(@PathVariable String id, @RequestBody UserAddVO userAddVO) {
        userService.editUser(id, userAddVO);
        return GenericResponse.success(userService.userBO2UserVO(userService.getUserById(id)));
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('deleteUser')")
    public GenericResponse deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
        return GenericResponse.success();
    }
}
