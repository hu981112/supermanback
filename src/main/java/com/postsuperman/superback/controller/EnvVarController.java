package com.postsuperman.superback.controller;

import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpTaskBO;
import com.postsuperman.superback.entity.PO.EnvVarPO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.service.EnvVarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@RequestMapping("/api/envvar")
public class EnvVarController {
    @Autowired
    private EnvVarService envVarService;

    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse list() {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        return GenericResponse.success(envVarService.getEnvVarByUser(userid));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse get(@PathVariable("id") @NotNull String id) {
        return  GenericResponse.success( envVarService.getEnvVar(id));
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse create(@RequestBody @Valid EnvVarPO envVarPO)  {
        envVarPO.setUserid(SecurityContextHolder.getContext().getAuthentication().getName());
        envVarService.addEnvVar(envVarPO);
        return GenericResponse.success();
    }

    @PostMapping("/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse update(@RequestBody @Valid EnvVarPO envVarPO) throws IOException {
        envVarService.updateEnvVar(envVarPO);
        return GenericResponse.success();
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GenericResponse delete(@PathVariable @Valid @NotNull String id) {
        envVarService.deleteEnvVar(id);
        return GenericResponse.success();
    }



}
