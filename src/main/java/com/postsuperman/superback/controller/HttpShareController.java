package com.postsuperman.superback.controller;

import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.common.PageModel;
import com.postsuperman.superback.entity.BO.HttpShareBO;
import com.postsuperman.superback.service.HttpShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api/share")
public class HttpShareController {
    @Autowired
    private HttpShareService httpShareService;
    @GetMapping("/list")
    public GenericResponse list() {
        return  GenericResponse.success(httpShareService.getHttpShareList());
    }
    @GetMapping("/listByPage")
    public GenericResponse list(String shareName,@NotNull Integer pageNo, @NotNull Integer pageSize) {
        PageModel res = httpShareService.getHttpShareListByPage(shareName, pageNo, pageSize);
        return  GenericResponse.success(res);
    }

    @GetMapping("/doc/{uuid}")
    public GenericResponse doclist(@PathVariable String uuid) {
        return GenericResponse.success(httpShareService.getHttpShare(uuid));
    }

    @GetMapping("/detail")
    public GenericResponse doc(@PathParam("uuid") String uuid, @PathParam("taskId") String taskId) {
        return GenericResponse.success(httpShareService.getHttpTaskDetail(uuid, taskId));
    }

    @PostMapping("/create")
    public GenericResponse create(@RequestBody HttpShareBO httpShare) {
        return GenericResponse.success(httpShareService.addHttpShare(httpShare));
    }

    @PostMapping("/update")
    public GenericResponse update(@RequestBody HttpShareBO httpShare) {
        httpShareService.updateHttpShare(httpShare);
        return GenericResponse.success();
    }

    @PostMapping("/delete/{id}")
    public GenericResponse delete(@PathVariable String id) {
        httpShareService.deleteHttpShare(id);
        return GenericResponse.success();
    }
}
