package com.postsuperman.superback.controller;

import com.postsuperman.superback.common.AccessLog;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.common.PageModel;
import com.postsuperman.superback.entity.PO.BatchTestPO;
import com.postsuperman.superback.service.BatchTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api/batchtest")
public class BatchTestController {
    @Autowired
    private  BatchTestService batchTestService;
    @GetMapping("/")
    public GenericResponse getBatchTests() {
        return GenericResponse.success(batchTestService.getBatchTestList());
    }
    @PostMapping("/create")
    public GenericResponse createBatchTest(@RequestBody BatchTestPO batchTestPO) {
        batchTestService.addBatchTest(batchTestPO);
        return GenericResponse.success();
    }

    @DeleteMapping("/{id}")
    public GenericResponse deleteBatchTest(@PathVariable String id) {
        batchTestService.deleteBatchTest(id);
        return GenericResponse.success();
    }

    @GetMapping("/do")
    public GenericResponse doBatchTest(@PathParam("id") String id,@PathParam("catchable") Boolean catchable,@PathParam("times") Integer times) {
        return GenericResponse.success(batchTestService.doBatchTest(id,catchable,times));
    }

    @PostMapping("/run")
    public GenericResponse doBatchTest(@RequestBody BatchTestPO batchTestPO) {
        return GenericResponse.success(batchTestService.doBatchTest(batchTestPO));
    }

}
