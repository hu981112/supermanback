package    com.postsuperman.superback.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
//import io.swagger.annotations.ApiOperation;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.common.LoginModel;
import com.postsuperman.superback.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;


@RestController
@RequestMapping("/api")
@Slf4j
public class AuthController {

    @Resource
    private AuthService authService;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/auth")
    public GenericResponse auth(@RequestBody @Validated LoginModel user) {
        String username = user.getUsername();
        String password = user.getPassword();

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "用户名或密码不能为空");
        }
        try {
            Map map= authService.login(username, password);
            return GenericResponse.success(map);
        } catch (GenericException e) {
            return GenericResponse.error(e);
        }
    }
    @PostMapping("/refresh")
    public GenericResponse refresh(@RequestHeader("auth") String token) {
        return GenericResponse.success(authService.refreshToken(token));
    }

}
