package com.postsuperman.superback.controller;

import com.postsuperman.superback.entity.data.MessageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@Slf4j
public class WSTaskController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/ws/broadcast")
    @SendTo("/broadcast/notification")
    public String broadcast(Principal principal, StompHeaderAccessor accessor, String message) {
//        messagingTemplate.convertAndSend("/user/queue/notification", "收到广播消息: " + message);
        return "收到来自 " + principal.getName() + " 的广播消息: " + message;
    }

    @MessageMapping("/ws/message")
    public void send(Principal principal, StompHeaderAccessor accessor, MessageInfo message) {
        log.info(message.getSender());
        log.info(message.getReceiver());
        log.info(principal.getName());
        messagingTemplate.convertAndSendToUser(message.getReceiver(), "/queue/notification", "收到来自 " + principal.getName() + " 的消息: " + message.getMessage());
    }
}
