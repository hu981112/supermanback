package com.postsuperman.superback.mapper;


import com.postsuperman.superback.entity.PO.UserPO;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<UserPO, String> {
    UserPO findDistinctById(String id);
    UserPO findDistinctFirstByUsername(String username);
    UserPO findDistinctFirstByPhone(String phone);
    List<UserPO> findByRolesContaining(String role);

}