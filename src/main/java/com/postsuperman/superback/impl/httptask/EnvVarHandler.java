package com.postsuperman.superback.impl.httptask;

import com.postsuperman.superback.common.SpringUtil;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.EnvVarPO;
import com.postsuperman.superback.service.EnvVarService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnvVarHandler  {
    private EnvVarService envVarService;
    EnvVarHandler() {
        this.envVarService = SpringUtil.getBean(EnvVarService.class);
    }

    public HttpReqPO handlerequest(HttpReqPO request) {
        List<EnvVarPO> envVarPOs = envVarService.getEnvVarByUser(request.getUserid());
        envVarPOs.forEach(System.out::println);
        //获取本机ip地址
        List<String>reglist=regEx("\\{[\\u4e00-\\u9fa5\\w]*?}",request.getAimurl());
        reglist.forEach(System.out::println);
        String aimurl=request.getAimurl();
        for (EnvVarPO envVarPO : envVarPOs) {
            for (String reg : reglist) {
                if (reg.equals("{"+envVarPO.getName()+"}")) {
                    aimurl=aimurl.replace(reg,envVarPO.getValue());
                }
            }
        }

        Integer index=aimurl.indexOf("127.0.0.1");
        if(index!=-1){
            Integer st1=aimurl.indexOf("//");
            Integer st2=aimurl.indexOf("/",st1==-1?0:st1+2);
            if(index<st2||st2==-1){
                aimurl = aimurl.replace("127.0.0.1", request.getSrcurl());
            }
        }
        request.setAimurl(aimurl);
        return request;
    }
    public static List<String> regEx(String patten,String textArea) {
        String pattern = patten;
        Pattern compile = Pattern.compile(pattern);
        Matcher matcher = compile.matcher(textArea);
        List<String> targetList = new ArrayList<String>();
        while (matcher.find()) {
            String substring = textArea.substring(matcher.start(), matcher.end());
            targetList.add(substring);
        }
        return targetList;
    }
    public static void main(String[] args) {
        String str = "{百度}:{port}";
        String regex = "\\{[\\u4e00-\\u9fa5\\w]*?}";
        regEx(regex,str).forEach(System.out::println);
    }
}
