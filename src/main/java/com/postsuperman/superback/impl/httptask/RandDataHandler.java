package com.postsuperman.superback.impl.httptask;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.entity.BO.HttpParmBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.mifmif.common.regex.Generex;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class RandDataHandler {

    public HttpReqPO handlerequest(HttpReqPO request) {
        List<HttpParmBO> headers = request.getHeader();
        if (headers != null) {
            request.setHeader(RandomGenerate(headers));
        }
        List<HttpParmBO> querys = request.getQuery();
        if (querys != null) {
            request.setQuery(RandomGenerate(querys));
        }
        if (isList(request.getBody())) {
            List<HttpParmBO> bodys = new ArrayList<>();
            for (Object o : (List<?>) request.getBody()) {
                bodys.add(JSONObject.parseObject(JSON.toJSONString(o), HttpParmBO.class));
            }
            if (bodys != null) {
                request.setBody(RandomGenerate(bodys));
            }
        }
        return request;
    }

    List<HttpParmBO> RandomGenerate(List<HttpParmBO> parms) {
        List<HttpParmBO> res = new ArrayList<>();
        for (HttpParmBO parm : parms) {
            String str = parm.getVal();
            if (!StringUtils.isEmpty(str) && str.startsWith("^") && str.endsWith("$")) {
                try {
                    Generex g = new Generex(str);
                    if (str.matches("\\^.*\\$")) {
                        String s = g.random();
                        parm.setVal(s.substring(1, s.length() - 2));
                    }
                } catch (Exception e) {
                    throw new GenericException(ExceptionType.SYSTEM_ERROR, "数据随机生成错误");
                }

            }
            res.add(parm);
        }
        return res;
    }

    //判断一个对象是否是列表
    private boolean isList(Object obj) {
        if (obj instanceof List) {
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        for (Integer i : list) {
            i = 2;
        }
        list.forEach(System.out::println);
    }
}
