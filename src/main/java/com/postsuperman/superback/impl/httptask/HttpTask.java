package com.postsuperman.superback.impl.httptask;

import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpResBO;
import com.postsuperman.superback.entity.BO.HttpTaskBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.HttpResPO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StopWatch;

import java.util.concurrent.Callable;

public class HttpTask  implements Callable<HttpTaskBO> {
    private HttpReqPO httpReqPO;

    public HttpTask(HttpReqPO httpReqPO) {
        this.httpReqPO = httpReqPO;
    }

    public HttpResPO run()  {
        EnvVarHandler envVarHandler=new EnvVarHandler();
        RandDataHandler randDataHandler=new RandDataHandler();
        HttpExecHandler httpExecHandler=new HttpExecHandler();
        this.httpReqPO =envVarHandler.handlerequest(this.httpReqPO);
        this.httpReqPO =randDataHandler.handlerequest(this.httpReqPO);
        return httpExecHandler.handlerequest(this.httpReqPO);
    }

    public HttpTaskBO getTaskResult(HttpResPO httpResPO)
    {
        HttpReqBO httpReqBO = new HttpReqBO();
        BeanUtils.copyProperties(this.httpReqPO,httpReqBO);
        httpReqBO.setUrl(this.httpReqPO.getAimurl());

        HttpResBO httpResBO = new HttpResBO();
        BeanUtils.copyProperties(httpResPO,httpResBO);

        HttpTaskBO httpTaskBO =HttpTaskBO.builder()
                .request(httpReqBO)
                .response(httpResBO)
                .build();
        return httpTaskBO;
    }
    @Override
    public HttpTaskBO call()  {
        if(this.httpReqPO ==null)
        {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"输入的httpReqBO为空");
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("HttpTask start running");
        HttpResPO httpResPO = run();
        stopWatch.stop();
        httpResPO.setTime(String.valueOf(stopWatch.getTotalTimeMillis()));
        return getTaskResult(httpResPO);
    }
}
