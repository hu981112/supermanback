package com.postsuperman.superback.impl.httptask;

import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.OkHttpUtil;
import com.postsuperman.superback.entity.BO.HttpParmBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.HttpResPO;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class HttpExecHandler  {
    public boolean isValidUrl(String url) {
        String regex = "^((http|https)://)?([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+([a-zA-Z0-9]{1,6})(:[0-9]{1,5})?(/.*)?";
        return url.matches(regex);
    }
    public static String getContent(Response response) {
        try {
            return response.body().string();
        } catch (IOException e) {
            throw new GenericException(ExceptionType.SYSTEM_ERROR, "返回结果解析失败");
        }
    }
    public HttpResPO handlerequest(HttpReqPO request) {
        if(!isValidUrl(request.getAimurl())){
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"url格式不正确");
        }
        request.setAimurl(getUrl(request.getAimurl(), request.getQuery()));
        Response response;
        try {
            switch (request.getMethod().toUpperCase()) {
                case "GET":
                   response= OkHttpUtil.get(request);
                   break;
                case "POST":
                    response= OkHttpUtil.post(request);
                    break;
                case "PUT":
                    response= OkHttpUtil.put(request);
                    break;
                case "DELETE":
                    response= OkHttpUtil.delete(request);
                    break;
                default:
                    throw new GenericException(ExceptionType.USER_INPUT_ERROR, "不支持的请求方法");
            }
        } catch (Exception e) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, e.getMessage());
        }
        HttpResPO httpResPO = HttpResPO.builder()
                .url(response.request().url().toString())
                .method(response.request().method())
                .header(response.headers().toString())
                .body(getContent(response))
                .contentType(response.body().contentType().toString())
                .status(String.valueOf(response.code()))
                .build();
        return httpResPO;
    }
    //根据query拼接生成get请求地址
    public static String getUrl(String url, List<HttpParmBO> params) {
        StringBuilder sb = new StringBuilder(url);
        if (params != null && params.size() > 0) {
            sb.append("?");
            for (HttpParmBO param : params) {
                sb.append(param.getKey()).append("=").append(param.getVal()).append("&");
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String regex = "^((http|https)://)?([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+([a-zA-Z0-9]{1,6})(:[0-9]{1,5})?(/.*)?";
        String url = "http://59.45.4.182:29546/api/users/auth";
        System.out.println(url.matches(regex));
    }
}
