package com.postsuperman.superback.impl;

import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.entity.PO.EnvVarPO;
import com.postsuperman.superback.service.EnvVarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Queue;

@Service
public class EnvVarServiceImpl implements EnvVarService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void addEnvVar(EnvVarPO envVarPO) {
        Query query = new Query();
        envVarPO.setId(null);
        query.addCriteria(Criteria.where("name").is(envVarPO.getName()));
        query.addCriteria(Criteria.where("userid").is(envVarPO.getUserid()));
        EnvVarPO envVar=mongoTemplate.findOne(query,EnvVarPO.class);
        if(envVar!=null){
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"envVar name is exist");
        }
        mongoTemplate.insert(envVarPO);
    }

    @Override
    public void updateEnvVar(EnvVarPO envVarPO) {
        String userid= SecurityContextHolder.getContext().getAuthentication().getName();
        if(envVarPO.getId()==null){
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"envVarId is null");
        }
        EnvVarPO envVar=mongoTemplate.findById(envVarPO.getId(),EnvVarPO.class);
        if(envVar==null||envVar.getUserid()==null||!envVar.getUserid().equals(userid)){
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"envVarId is not exist or not belong to current user");
        }
        envVarPO.setUserid(userid);
        mongoTemplate.save(envVarPO);
    }

    @Override
    public void deleteEnvVar(String id) {
        String userid= SecurityContextHolder.getContext().getAuthentication().getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        query.addCriteria(Criteria.where("userid").is(userid));
        mongoTemplate.remove(query,EnvVarPO.class);
    }

    @Override
    public EnvVarPO getEnvVar(String id) {
        String userid= SecurityContextHolder.getContext().getAuthentication().getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        query.addCriteria(Criteria.where("userid").is(userid));
        return mongoTemplate.findOne(query,EnvVarPO.class);
    }

    @Override
    public List<EnvVarPO> getEnvVars() {
        return mongoTemplate.findAll(EnvVarPO.class);
    }

    @Override
    public List<EnvVarPO> getEnvVarByUser(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userid").is(userId));
        return mongoTemplate.find(query,EnvVarPO.class);
    }
}
