package com.postsuperman.superback.impl;

import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.ObjectUtil;
import com.postsuperman.superback.entity.BO.UserBO;
import com.postsuperman.superback.entity.PO.UserPO;
import com.postsuperman.superback.entity.VO.UserAddVO;
import com.postsuperman.superback.entity.VO.UserVO;
import com.postsuperman.superback.mapper.UserRepository;
import com.postsuperman.superback.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private final static Map roleMap=new HashMap();


    @Autowired
    public void UserServiceImpl(UserRepository userRepository,
                                PasswordEncoder passwordEncoder) {
        this.userRepository=userRepository;
        this.passwordEncoder=passwordEncoder;
        roleMap.put(0,"ADMIN");
        roleMap.put(1,"USER");
    }

    private UserPO getStaticUser(String username)
    {
        if("superman".equals(username))
        {
            UserPO userPO=UserPO.builder()
                    .username("superman")
                    .id("0")
                    .password(passwordEncoder.encode("123456"))
                    .Roles(Arrays.asList("ADMIN"))
                    .enabled(true)
                    .build();
            return userPO;
        }
        else
            return null;
    }

    @Override
    public UserBO getUserById(String  id) {
        UserPO userPO = userRepository.findDistinctById(id);
        if (userPO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不存在");
        }
        return userPO2UserBO(userPO);
    }

    @Override
    public UserBO getUserByUsername(String username) {
        return (UserBO) loadUserByUsername(username);
    }

    @Override
    public UserBO getUserByPhone(String phone) {
        return userPO2UserBO(userRepository.findDistinctFirstByPhone(phone));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if (s == null || StringUtils.isBlank(s)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户名不能为空");
        }
        UserPO userPO = getStaticUser(s);
        if(userPO!=null)
            return userPO2UserBO(userPO);
        userPO=userRepository.findDistinctFirstByUsername(s);
        if (userPO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不存在");
        }
        return userPO2UserBO(userPO);
    }

    @Override
    public List<UserBO> getAllUsers() {
        List<UserBO> userBOS = new ArrayList<>();
        userRepository.findAll().forEach(u -> userBOS.add(userPO2UserBO(u)));
        return userBOS;
    }

    @Override
    public List<UserBO> getUsersByRole(String role) {
        if (role == null || StringUtils.isBlank(role)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "角色不能为空");
        }
        List<UserBO> userBOS = new ArrayList<>();
        userRepository.findByRolesContaining(role).forEach(u -> userBOS.add(userPO2UserBO(u)));;
        return userBOS;
    }

    @Override
    @Transactional
    public String saveUser(UserAddVO userAddVO) throws GenericException {
        if (userAddVO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不能为空");
        }

        UserPO userPO = new UserPO();
        BeanUtils.copyProperties(userAddVO, userPO);
        userPO.setPassword(passwordEncoder.encode(userPO.getPassword()));

        List<String> roles = userAddVO.getRoles().stream().map(r->roleMap.get(r).toString()).filter(data->data!=null).collect(Collectors.toList());
        if (roles.size()==0) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "权限不合法");
        }
        userPO.setRoles(roles);

        if (userRepository.findDistinctFirstByUsername(userPO.getUsername()) != null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户已存在");
        }

        if (userRepository.findDistinctFirstByPhone(userPO.getPhone()) != null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "手机号已使用");
        }
        userPO.setCreateTime(new Date());
        userPO=userRepository.insert(userPO);
        return userPO.getId();
    }

    @Override
    public String registerUser(UserAddVO userAddVO) {
        return saveUser(userAddVO);
    }

    @Override
    public void editUser(String id, UserAddVO userAddVO) throws GenericException {
        if (id == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户 id 不能为空");
        }
        if (userAddVO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不能为空");
        }

        if (userRepository.findDistinctById(id) == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不存在");
        }

        UserPO userPO = new UserPO();
        BeanUtils.copyProperties(userAddVO, userPO);
        userPO.setCreateTime(new Date());

        if (userPO.getPassword() != null) {
            userPO.setPassword(passwordEncoder.encode(userPO.getPassword()));
        }

        List<String> roles = userAddVO.getRoles().stream().map(r->roleMap.get(r).toString()).filter(data->data!=null).collect(Collectors.toList());

        userPO.setRoles(roles);

        if (StringUtils.isNotEmpty(userAddVO.getPhone())) {
            UserPO userPO1 = userRepository.findDistinctFirstByPhone(userAddVO.getPhone());
            if (userPO1 != null && !userPO1.getId().equals(id)) {
                throw new GenericException(ExceptionType.USER_INPUT_ERROR, "手机号已使用");
            }
        }

        if (!ObjectUtil.allFieldIsNULL(userPO)) {
            userPO.setId(id);
            userRepository.save(userPO);
        }
    }

    @Override
    public void deleteUser(String id) throws GenericException {
        if (id == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户 id 不能为空");
        }
        if (userRepository.findDistinctById(id) == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不存在");
        }
        String current = ((UserBO) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        if (id.equals(current)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户不能删除自己");
        }
        userRepository.deleteById(id);
    }

    @Override
    public Boolean isUserExist(String id) {
        return userRepository.existsById(id);
    }



    private UserBO userPO2UserBO(UserPO userPO) {
        if (userPO == null) {
            return null;
        }
        UserBO userBO = new UserBO();

        BeanUtils.copyProperties(userPO, userBO);

        List<String> authorities = userPO.getRoles();

        userBO.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList(String.join(",", authorities)));
        return userBO;
    }

    public UserVO userBO2UserVO(UserBO userBO) {
        if (userBO == null) {
            return null;
        }
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userBO, userVO);
        Long correctTime = userVO.getCreateTime().getTime() + 8 * 60 * 60 * 1000;
        userVO.setCreateTime(new Date(correctTime));
        return userVO;
    }
}
