package com.postsuperman.superback.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.postsuperman.superback.common.AddressIpUtils;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.ObjectUtil;
import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpTaskBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.impl.httptask.HttpTask;
import com.postsuperman.superback.service.HttpTaskService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutorService;

@Service
public class HttpTaskServiceImpl implements HttpTaskService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Resource(name = "ThreadPoolExecutor")
    private ExecutorService executorService;

    HttpTaskPO HttpTaskBO2PO(HttpTaskBO httpTaskBO) {
        String uerId = SecurityContextHolder.getContext().getAuthentication().getName();
        if (StringUtils.isEmpty(uerId) || StringUtils.isBlank(uerId)) {
            throw new GenericException(ExceptionType.FORBIDDEN_ERROR, "用户未登录");
        }
        HttpTaskPO httpTaskPO = new HttpTaskPO();
        BeanUtils.copyProperties(httpTaskBO, httpTaskPO);
        httpTaskPO.setUserid(uerId);
        httpTaskPO.setTime(new Date());
        return httpTaskPO;
    }

    @Override
    public void addTask(HttpTaskBO httpTaskBO) {
        HttpTaskPO httpTaskPO = HttpTaskBO2PO(httpTaskBO);
        mongoTemplate.insert(httpTaskPO);
    }

    @Override
    public void updateTask(HttpTaskPO httpTaskPO) {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        if (httpTaskPO.getId() == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "id不能为空");
        }
        HttpTaskPO httptask = mongoTemplate.findById(httpTaskPO.getId(), HttpTaskPO.class);
        if (httptask == null || !httptask.getUserid().equals(userid)) {
            throw new GenericException(ExceptionType.FORBIDDEN_ERROR, "没有权限修改");
        }
        httptask.setUserid(userid);
        mongoTemplate.save(httpTaskPO);
    }

    @Override
    public void deleteTask(String taskId) {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(taskId));
        query.addCriteria(Criteria.where("userid").is(userid));
        mongoTemplate.remove(query, HttpTaskPO.class);
    }

    @Override
    public HttpTaskPO getTask(String taskId) {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(taskId));
        query.addCriteria(Criteria.where("userid").is(userid));
        return mongoTemplate.findOne(query, HttpTaskPO.class);
    }

    @Override
    public List getTaskList(HttpTaskPO httpTaskPO) {
        Query query = new Query();
        if (httpTaskPO.getUserid() != null) {
            query.addCriteria(Criteria.where("userid").is(httpTaskPO.getUserid()));
        }
        if (httpTaskPO.getTaskname() != null) {
            query.addCriteria(Criteria.where("taskname").is(httpTaskPO.getTaskname()));
        }
        if (httpTaskPO.getRequest() != null) {
            query.addCriteria(Criteria.where("request").is(httpTaskPO.getRequest()));
        }
        if (httpTaskPO.getResponse() != null) {
            query.addCriteria(Criteria.where("response").is(httpTaskPO.getResponse()));
        }
        return mongoTemplate.find(query, HttpTaskPO.class);
    }

    @Override
    public Integer getTaskCount() {
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("userid").is(userid));
        return (int) mongoTemplate.count(query, HttpTaskPO.class);
    }

    @Override
    public List<HttpTaskPO> getTaskVOByUser(String userId) {
        return mongoTemplate.find(new Query(Criteria.where("userid").is(userId)), HttpTaskPO.class);

        //使用userid进行聚合查询，相同url作为一组

//        System.out.println(userId);
//
//        List<AggregationOperation> operations = new ArrayList<>();
//        operations.add(Aggregation.match(Criteria.where("userid").is(userId)));
//        operations.add(Aggregation.group("request.url").first("request.url").as("url"));
//        operations.add(Aggregation.group("_id").
//        operations.add(Aggregation.group("taskname").addToSet("taskname").as("cases"));
//
//        Aggregation aggregation = Aggregation.newAggregation(operations);
//        AggregationResults<HttpTaskListVO>results = mongoTemplate.aggregate(aggregation, HttpTaskPO.class, HttpTaskListVO.class);
//        List<HttpTaskListVO> all=results.getMappedResults();
//        all.forEach(System.out::println);
//        return all;
    }

    //检验字符串是否是合法url

    @Override
    public HttpTaskBO RunTask(HttpReqBO httpReqBO) {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        HttpReqPO httpReqPO = new HttpReqPO();
        BeanUtils.copyProperties(httpReqBO, httpReqPO);
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        httpReqPO.setUserid(userid);
        httpReqPO.setAimurl(httpReqBO.getUrl());
        String sourceurl = AddressIpUtils.getIpAdrress(request);
        httpReqPO.setSrcurl(sourceurl);
        HttpTask httpTask = new HttpTask(httpReqPO);
        return httpTask.call();
//        Future<HttpResPO> httpResBOFuture= executorService.submit(httpTask);
//        try {
//            return httpResBOFuture.get();
//        }
//        catch (Exception e){
//            throw new GenericException(ExceptionType.SYSTEM_ERROR,e.getMessage());
//        }
    }

    @Override
    public byte[] downloadFile(String id) {
        if (id == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "id不能为空");
        }
        HttpTaskPO httpTaskPO = getTask(id);
        if (httpTaskPO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "id不存在");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String json = null;
        try {
            json = objectMapper.writeValueAsString(httpTaskPO);
        } catch (JsonProcessingException e) {
            throw new GenericException(ExceptionType.SYSTEM_ERROR, e.getMessage());
        }
        //将json转换为byte数组
        byte[] bytes = new byte[0];
        try {
            bytes = json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bytes;


    }

    @Override
    public void uploadFile(MultipartFile file) {
        if (file == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "文件不能为空");
        }
        //读取file所有字节并转成json
        String json = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            json = new String(file.getBytes(), "UTF-8");
        } catch (IOException e) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "文件读取失败");
        }
        HttpTaskPO httpTaskPO= null;
        try {
            httpTaskPO = objectMapper.readValue(json, HttpTaskPO.class);
        } catch (JsonProcessingException e) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "文件解析失败");
        }
        if (ObjectUtil.allFieldIsNULL(httpTaskPO)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "文件内容不能为空");
        }
        httpTaskPO.setTime(new Date());
        httpTaskPO.setUserid(SecurityContextHolder.getContext().getAuthentication().getName());
        httpTaskPO.setId(null);
        //将httpTaskPO保存到数据库
        mongoTemplate.save(httpTaskPO);
    }



}
