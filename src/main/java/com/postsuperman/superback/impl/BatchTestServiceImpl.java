package com.postsuperman.superback.impl;

import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpResBO;
import com.postsuperman.superback.entity.PO.BatchTestPO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.impl.httptask.HttpTask;
import com.postsuperman.superback.service.BatchTestService;
import com.postsuperman.superback.service.HttpTaskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;


@Service
public class BatchTestServiceImpl implements BatchTestService {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private HttpTaskService httpTaskSerive;
    @Autowired
    ThreadPoolExecutor threadPoolExecutor;

    @Override
    public void addBatchTest(BatchTestPO batchTestPO) {
       if(batchTestPO == null)
           throw new GenericException(ExceptionType.USER_INPUT_ERROR,"batchTestPO is null");
       if(batchTestPO.getTasks().size() == 0)
              throw new GenericException(ExceptionType.USER_INPUT_ERROR,"batchTestPO.getTasks().size() == 0");
       if(StringUtils.isBlank(batchTestPO.getTestName()))
           throw new GenericException(ExceptionType.USER_INPUT_ERROR,"batchTestPO.getName() is blank");
       Query query = new Query();
       String userid = SecurityContextHolder.getContext().getAuthentication().getName();
       query.addCriteria(Criteria.where("userid").is(userid));
       List<String> ids = batchTestPO.getTasks().stream().map(HttpTaskPO::getId).collect(Collectors.toList());
       query.addCriteria(Criteria.where("id").in(ids));
       if(mongoTemplate.count(query,HttpTaskPO.class) != batchTestPO.getTasks().size())
           throw new GenericException(ExceptionType.NOT_FOUND, "部分接口用例未找到或没有权限访问");
       batchTestPO.setUserid(userid);
       batchTestPO.setCreateTime(new Date());
       mongoTemplate.save(batchTestPO);
    }

    @Override
    public void deleteBatchTest(String id) {
        if(StringUtils.isBlank(id))
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"id is blank");
        if(mongoTemplate.findById(id, BatchTestPO.class) == null)
            throw new GenericException(ExceptionType.NOT_FOUND,"批量测试不存在");
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        query.addCriteria(Criteria.where("userid").is(SecurityContextHolder.getContext().getAuthentication().getName()));
        mongoTemplate.remove(query,BatchTestPO.class);
    }

    @Override
    public List<BatchTestPO> getBatchTestList() {
        Query query = new Query();
        String userid = SecurityContextHolder.getContext().getAuthentication().getName();
        query.addCriteria(Criteria.where("userid").is(userid));
        return mongoTemplate.find(query,BatchTestPO.class);
    }
    @Override
    public BatchTestPO doBatchTest(String id, Boolean catchable, Integer times) {
        if(StringUtils.isBlank(id))
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"id is blank");
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        query.addCriteria(Criteria.where("userid").is(SecurityContextHolder.getContext().getAuthentication().getName()));
        BatchTestPO batchTestPO = mongoTemplate.findOne(query,BatchTestPO.class);
        if(batchTestPO == null)
            throw new GenericException(ExceptionType.NOT_FOUND,"批量测试不存在");
        if(times == null)
        {
            times=1;
        }
        else if(times>=5)
        {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"不允许过多的重复测试");
        }
        if(catchable == null)
        {
            catchable = false;
        }
        List<HttpTaskPO> tasks = new ArrayList<>();
        for(Integer i=0;i<times;i++) {
            Boolean flag = false;
            for (HttpTaskPO httpTaskPO : batchTestPO.getTasks()) {
                HttpResBO httpResBO = new HttpResBO();
                try {
                    httpResBO = httpTaskSerive.RunTask(httpTaskPO.getRequest()).getResponse();
                } catch (Exception e) {
                    httpResBO.setUrl(httpTaskPO.getRequest().getUrl());
                    httpResBO.setBody(e.getMessage());
                    flag=true;
                }
                httpTaskPO.setResponse(httpResBO);
                tasks.add(httpTaskPO);
                if(flag&&catchable)
                {
                    break;
                }
            }
            if(flag&&catchable)
            {
                break;
            }
        }
        batchTestPO.setTasks(tasks);
        return batchTestPO;
    }
    @Override
    public BatchTestPO doBatchTest(BatchTestPO batchTestPO) {
        if(batchTestPO == null)
            throw new GenericException(ExceptionType.NOT_FOUND,"批量测试不存在");
        if(batchTestPO.getTasks().size() == 0)
            throw new GenericException(ExceptionType.USER_INPUT_ERROR,"batchTestPO.getTasks().size() == 0");
        for(HttpTaskPO httpTaskPO:batchTestPO.getTasks()){
            HttpResBO httpResBO = new HttpResBO();
            try {
                httpResBO = httpTaskSerive.RunTask(httpTaskPO.getRequest()).getResponse();
            }catch (Exception e){
                httpResBO.setUrl(httpTaskPO.getRequest().getUrl());
                httpResBO.setBody(e.getMessage());
            }
            httpTaskPO.setResponse(httpResBO);
        }
        return batchTestPO;
    }
}

