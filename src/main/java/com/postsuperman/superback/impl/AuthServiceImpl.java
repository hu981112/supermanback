package com.postsuperman.superback.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.JwtTokenUtil;
import com.postsuperman.superback.entity.BO.UserBO;
import com.postsuperman.superback.service.AuthService;
import com.postsuperman.superback.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service(value = "rbacService")
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private UserService userService;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private JwtTokenUtil util;

    @Override
    public Map login(String username, String password) throws GenericException {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (AuthenticationException e) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "用户名或密码不正确");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        Map map = new HashMap<>();
        map.put("token", util.generateToken(userDetails));
        map.put("id", ((UserBO)userDetails).getId());
        return map;
    }

    @Override
    public String refreshToken(String oldToken) {
        boolean isExpire = util.isTokenExpired(oldToken);
        if (isExpire) {
            return util.refreshToken(oldToken);
        }
        return null;
    }

}
