package com.postsuperman.superback.impl;

import com.postsuperman.superback.common.*;
import com.postsuperman.superback.entity.BO.HttpShareBO;
import com.postsuperman.superback.entity.PO.HttpSharePO;
import com.postsuperman.superback.entity.PO.HttpTaskPO;
import com.postsuperman.superback.entity.VO.HttpShareVO;
import com.postsuperman.superback.entity.VO.HttpTaskVO;
import com.postsuperman.superback.service.HttpShareService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class HttpShareServiceImpl implements HttpShareService {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public HttpShareServiceImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public HttpShareVO getHttpShare(String uuid) {
        if(StringUtils.isBlank(uuid)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "uuid is blank");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("uuid").is(uuid));
        HttpSharePO httpSharePO= mongoTemplate.findOne(query, HttpSharePO.class);
        if (httpSharePO == null) {
            throw new GenericException(ExceptionType.NOT_FOUND, "没有找到要分享的文档");
        }
        query = new Query();
        query.addCriteria(Criteria.where("id").in(httpSharePO.getTaskIds()));
        List<HttpTaskPO> httpTaskPOS = mongoTemplate.find(query, HttpTaskPO.class);
        if (httpTaskPOS == null || httpTaskPOS.isEmpty()) {
            throw new GenericException(ExceptionType.NOT_FOUND, "要分享的文档没有接口用例");
        }
        List<HttpTaskVO>httpTaskVOS = httpTaskPOS.stream().map(httpTaskPO -> {
            HttpTaskVO httpTaskVO = new HttpTaskVO();
            BeanUtils.copyProperties(httpTaskPO, httpTaskVO);
            if(httpTaskPO.getRequest() != null) {
                httpTaskVO.setUrl(httpTaskPO.getRequest().getUrl());
            }
            return httpTaskVO;
                }).collect(Collectors.toList());
        HttpShareVO httpShareVO = new HttpShareVO();
        BeanUtils.copyProperties(httpSharePO, httpShareVO);
        httpShareVO.setTasks(httpTaskVOS);
        return httpShareVO;
    }

    @Override
    public HttpTaskPO getHttpTaskDetail(String uuid, String taskId) {
        if(StringUtils.isBlank(uuid)||StringUtils.isBlank(taskId)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "uuid or taskId is blank");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("uuid").is(uuid));
        HttpSharePO httpSharePO= mongoTemplate.findOne(query, HttpSharePO.class);
        if (httpSharePO == null) {
            throw new GenericException(ExceptionType.NOT_FOUND, "没有找到要分享的文档");
        }
        if(!httpSharePO.getTaskIds().contains(taskId)) {
            throw new GenericException(ExceptionType.NOT_FOUND, "没有找到要分享的接口用例");
        }
        return mongoTemplate.findById(taskId, HttpTaskPO.class);
    }

    @Override
    public HttpSharePO addHttpShare(HttpShareBO httpShareBO) {
        if(httpShareBO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "httpShareBO is null");
        }
        if(StringUtils.isBlank(httpShareBO.getShareName())) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "shareName is blank");
        }
        if(httpShareBO.getTaskIds()== null || httpShareBO.getTaskIds().isEmpty()) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "tasks is empty");
        }
        Criteria criteria= Criteria.where("id").in(httpShareBO.getTaskIds());
        criteria.and("userid").is(SecurityContextHolder.getContext().getAuthentication().getName());
        Query query = new Query(criteria);
        long count = mongoTemplate.count(query, HttpTaskPO.class);
        if(count != httpShareBO.getTaskIds().size()) {
            throw new GenericException(ExceptionType.NOT_FOUND, "部分接口用例未找到或没有权限访问");
        }
        HttpSharePO httpSharePO = HttpSharePO.builder()
                .uuid(UUID.randomUUID().toString().replace("-", ""))
                .shareName(httpShareBO.getShareName())
                .taskIds(httpShareBO.getTaskIds())
                .userId(SecurityContextHolder.getContext().getAuthentication().getName())
                .createTime(new Date())
                .build();;
        httpSharePO=mongoTemplate.insert(httpSharePO);
        return httpSharePO;


        
    }
    @Override
    public List getHttpShareList() {
        Criteria criteria= Criteria.where("userId").is(SecurityContextHolder.getContext().getAuthentication().getName());
        Query query = new Query(criteria);
        List<HttpSharePO> httpSharePOS = mongoTemplate.find(query, HttpSharePO.class);
        return httpSharePOS;
    }
    @Override
    public PageModel getHttpShareListByPage(String shareName,Integer pageNo,Integer pageSize) {
        Query query = new Query();
        if(StringUtils.isNotBlank(shareName))
            query.addCriteria(Criteria.where("shareName").regex(".*?" + shareName + ".*?"));
        query.addCriteria(Criteria.where("userId").is(SecurityContextHolder.getContext().getAuthentication().getName()));
        Long count = mongoTemplate.count(query, HttpSharePO.class);
        query.skip((pageNo - 1) * pageSize);
        query.limit(pageSize);
        List datas = mongoTemplate.find(query,HttpSharePO.class);
        PageModel pageModel = PageModel.builder()
                .datas(datas)
                .pageNo(pageNo)
                .pageSize(pageSize)
                .rowCounts(count)
                .build();
        return pageModel;
    }

    @Override
    public void deleteHttpShare(String id) {
        if(StringUtils.isBlank(id)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "id is blank");
        }
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        HttpSharePO httpSharePO = mongoTemplate.findById(id, HttpSharePO.class);
        if(httpSharePO == null) {
            throw new GenericException(ExceptionType.NOT_FOUND, "没有找到要删除的分享");
        }
        if(!httpSharePO.getUserId().equals(userId)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "没有权限删除该分享");
        }
        mongoTemplate.remove(httpSharePO);
    }

    @Override
    public void updateHttpShare(HttpShareBO httpShareBO) {
        if (httpShareBO == null) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "httpShareBO is null");
        }
        if (StringUtils.isBlank(httpShareBO.getId())) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "id is blank");
        }
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        HttpSharePO httpSharePO = mongoTemplate.findById(httpShareBO.getId(), HttpSharePO.class);
        if (httpSharePO == null) {
            throw new GenericException(ExceptionType.NOT_FOUND, "没有找到要更新的分享");
        }
        if (!httpSharePO.getUserId().equals(userId)) {
            throw new GenericException(ExceptionType.USER_INPUT_ERROR, "没有权限更新该分享");
        }
        if (StringUtils.isNotBlank(httpShareBO.getShareName())){
            httpSharePO.setShareName(httpShareBO.getShareName());
        }
        if(httpShareBO.getTaskIds() != null && !httpShareBO.getTaskIds().isEmpty()) {
            Criteria criteria= Criteria.where("id").in(httpShareBO.getTaskIds());
            criteria.and("userid").is(SecurityContextHolder.getContext().getAuthentication().getName());
            Query query = new Query(criteria);
            long count = mongoTemplate.count(query, HttpTaskPO.class);
            if(count != httpShareBO.getTaskIds().size()) {
                throw new GenericException(ExceptionType.NOT_FOUND, "部分接口用例未找到或没有权限访问");
            }
            httpSharePO.setTaskIds(httpShareBO.getTaskIds());
        }
        mongoTemplate.save(httpSharePO);
    }


}
