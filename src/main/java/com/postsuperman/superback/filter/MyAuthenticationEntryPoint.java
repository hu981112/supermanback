package com.postsuperman.superback.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.postsuperman.superback.common.AccessLog;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final MongoTemplate mongoTemplate;
//
    @Autowired
   public MyAuthenticationEntryPoint(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf8");
        ObjectMapper mapper = new ObjectMapper();
        GenericResponse body = GenericResponse.error(ExceptionType.TOKEN_EXPIRED, "资源需要进行验证才可以访问");
        AccessLog accessLog = AccessLog.getInstance(httpServletRequest, httpServletResponse, body);
//        log.info(accessLog.toString());
        mongoTemplate.save(accessLog);
        httpServletResponse.setStatus(401);
        httpServletResponse.getWriter().write(mapper.writeValueAsString(body));
    }
}
