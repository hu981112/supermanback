package com.postsuperman.superback.filter;


import com.postsuperman.superback.common.GenericResponse;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
@ControllerAdvice
public class GlobalResponseAdvice implements ResponseBodyAdvice {


    private static final List<String> BLOCK_LIST = Arrays.asList(
            "/static",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/v3/api-docs",
            "/webjars/**",
            "/test/**",
            "/api/captcha",
            "/api/auth_captcha",
            "/api/**/config/**"
    );

    private boolean isBlockListContain(String path) {
        AntPathMatcher matcher = new AntPathMatcher();
        for (String block : BLOCK_LIST) {
            if (matcher.match(block, path)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object beforeBodyWrite(Object body,
                                           MethodParameter returnType,
                                           MediaType selectedContentType,
                                           Class selectedConverterType,
                                           ServerHttpRequest request,
                                           ServerHttpResponse response) {
        if (body instanceof GenericResponse) {
            String path = request.getURI().getPath();

            if (!isBlockListContain(path)) {
                HttpServletRequest httpServletRequest =
                        ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                                .getRequest();

                httpServletRequest.setAttribute("body", body);

//            HttpSession httpSession = httpServletRequest.getSession(true);
//            httpSession.setAttribute("body", body);

            }
            if (((GenericResponse) body).getStatus() != 999) {
                response.setStatusCode(
                        HttpStatus.valueOf(((GenericResponse) body).getStatus())
                );
            } else {
                response.setStatusCode(
                        HttpStatus.INTERNAL_SERVER_ERROR
                );
            }

        }

        return body;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }
}
