package com.postsuperman.superback.filter;



import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericException;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.entity.PO.ExceptionLogPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import java.util.List;


@ControllerAdvice
@Slf4j
public class WebExceptionHandler {

    @ExceptionHandler(GenericException.class)
    @ResponseBody
    public GenericResponse handleGenericException(GenericException e) {
        if (e.getCode() != ExceptionType.USER_INPUT_ERROR.getCode() && e.getCode() != ExceptionType.FORBIDDEN_ERROR.getCode() && e.getCode() != ExceptionType.TOKEN_EXPIRED.getCode()) {
            //持久化系统异常
            ExceptionLogPO exceptionLogPO = ExceptionLogPO.getInstance(e);
            log.error("Message: {}\nStacktrace:\n{}", exceptionLogPO.getMessage(), exceptionLogPO.getStacktrace());
        }
        return GenericResponse.error(e);
    }

    private String getMessage(BindingResult br) {
        StringBuilder msgStr = new StringBuilder();
        List<FieldError> feList = br.getFieldErrors();
        for (FieldError fe : feList) {
            String message = fe.getDefaultMessage();
            String field = fe.getField();
            msgStr.append(field).append(" ").append(message).append("; ");
        }
        return msgStr.toString();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public GenericResponse handleBindException(MethodArgumentNotValidException e) {
        return GenericResponse.error(new GenericException(ExceptionType.USER_INPUT_ERROR, "参数异常: " + getMessage(e.getBindingResult())));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseBody
    public GenericResponse handleBindException(MethodArgumentTypeMismatchException e) {
        return GenericResponse.error(new GenericException(ExceptionType.USER_INPUT_ERROR, "参数异常: " + e.getMessage()));
    }

    @ExceptionHandler(BindException.class)
    @ResponseBody
    public GenericResponse handleBindException(BindException e) {
        return GenericResponse.error(new GenericException(ExceptionType.USER_INPUT_ERROR, "参数异常: " + getMessage(e.getBindingResult())));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public GenericResponse handleBindException(ConstraintViolationException e) {
        return GenericResponse.error(new GenericException(ExceptionType.USER_INPUT_ERROR, "参数异常: " + e.getMessage()));
    }


    @ExceptionHandler(DataAccessException.class)
    @ResponseBody
    public GenericResponse handleDataAccessException(DataAccessException e) {
        ExceptionLogPO exceptionLogPO = ExceptionLogPO.getInstance(e);
        log.error("Message: {}\nStacktrace:\n{}", exceptionLogPO.getMessage(), exceptionLogPO.getStacktrace());
        return GenericResponse.error(ExceptionType.SYSTEM_ERROR, "数据库异常: " + e.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public GenericResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "请求参数错误: " + e.getMessage());
    }

    @ExceptionHandler(ReflectiveOperationException.class)
    @ResponseBody
    public GenericResponse handleReflectionException(ReflectiveOperationException e) {
        return GenericResponse.error(ExceptionType.SYSTEM_ERROR, "反射异常: " + e.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public GenericResponse handleMethodException(HttpRequestMethodNotSupportedException e) {
        return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "请求方法错误: " + e.getMessage());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    public GenericResponse handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "请求参数错误: " + e.getMessage());
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    @ResponseBody
    public GenericResponse handleMissingServletRequestPartException(MissingServletRequestPartException e) {
        return GenericResponse.error(ExceptionType.USER_INPUT_ERROR, "请求参数错误: " + e.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public GenericResponse handleNullPointerException(NullPointerException e) {
        ExceptionLogPO exceptionLogPO = ExceptionLogPO.getInstance(e);
        log.error("Message: {}\nStacktrace:\n{}", exceptionLogPO.getMessage(), exceptionLogPO.getStacktrace());
        return GenericResponse.error(ExceptionType.SYSTEM_ERROR, "空指针异常: " + e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public GenericResponse handleException(Exception e) {
        if (e instanceof NoHandlerFoundException) {
            return GenericResponse.error(ExceptionType.NOT_FOUND, "Not Found");
        } else {
            ExceptionLogPO exceptionLogPO = ExceptionLogPO.getInstance(e);
            log.error("Message: {}\nStacktrace:\n{}", exceptionLogPO.getMessage(), exceptionLogPO.getStacktrace());
            return GenericResponse.error(ExceptionType.UNKNOWN_ERROR, "出现未知错误请联系管理员: \n" + e.getMessage());
        }
    }

}
