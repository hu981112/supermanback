package com.postsuperman.superback.filter;


import com.postsuperman.superback.common.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.security.Principal;

@Component
@Slf4j
public class WebSocketChannelInterceptor implements ChannelInterceptor {

    @Resource
    private JwtTokenUtil util;

    @Resource
    private UserDetailsService userDetailsService;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = StompHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            String token = accessor.getNativeHeader("auth").get(0);
            String requestId = accessor.getNativeHeader("id").get(0);
            log.info(token);
            if (token != null && !StringUtils.isEmpty(token)) {
                String username = util.getUsernameFromToken(token);
                if (username != null) {
                    UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                    if (util.isTokenExpired(token)) {
                        return null;
                    }
                    if (util.validateToken(token, userDetails)) {
                        RequestUser user = new RequestUser(username);
                        accessor.setUser(user);
                        return message;
                    }
                }

            }
        }
        return message;
    }

    private static class RequestUser implements Principal {

        private String id;

        public RequestUser(String id) {
            this.id = id;
        }

        @Override
        public String getName() {
            return id;
        }
    }
}
