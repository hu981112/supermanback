package com.postsuperman.superback.filter;


import com.postsuperman.superback.common.AccessLog;
import com.postsuperman.superback.common.AddressIpUtils;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.common.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.Date;

@Component
@Slf4j
public class AccessLogInterceptor implements HandlerInterceptor {

    //请求开始时间标识
    private static final String LOGGER_SEND_TIME = "SEND_TIME";
    //请求日志实体标识
    private static final String LOGGER_ACCESSLOG = "ACCESSLOG_ENTITY";

    private final JwtTokenUtil jwtTokenUtil;

    private final MongoTemplate mongoTemplate;

    @Autowired
    public AccessLogInterceptor(JwtTokenUtil jwtTokenUtil, MongoTemplate mongoTemplate) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //创建日志实体
        AccessLog accessLog = new AccessLog();

        // 设置IP地址
        accessLog.setIp(AddressIpUtils.getIpAdrress(request));


        //设置请求方法,GET,POST...
        accessLog.setHttpMethod(request.getMethod());

        //设置请求路径
        accessLog.setUrl(request.getRequestURI());

        //设置请求开始时间
        request.setAttribute(LOGGER_SEND_TIME,System.currentTimeMillis());

        //设置请求实体到request内，方便afterCompletion方法调用
        request.setAttribute(LOGGER_ACCESSLOG,accessLog);
        return true;
    }



    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //获取本次请求日志实体
        AccessLog accessLog = (AccessLog) request.getAttribute(LOGGER_ACCESSLOG);

        //获取请求错误码，根据需求存入数据库，这里不保存
        int status = response.getStatus();
        accessLog.setHttpStatus(status);

        try {
            GenericResponse body = (GenericResponse) request.getAttribute("body");

            String message = body.getMessage();

            accessLog.setMessage(message);
        } catch (Exception e) {
            accessLog.setMessage("");
        }


        String token = request.getHeader("auth");

        try {
            BufferedReader reader = request.getReader();

            String requestBody = IOUtils.toString(reader);
            if (requestBody.length() > 2048) {
                accessLog.setRequestBody(requestBody.substring(0, 2048));
            } else {
                accessLog.setRequestBody(requestBody);
            }

        } catch (Exception ignore) {
        }

        accessLog.setQueryString(request.getQueryString());

        accessLog.setUsername(jwtTokenUtil.getUsernameFromToken(token));

        //当前时间
        long currentTime = System.currentTimeMillis();

        //请求开始时间
        long sendTime = (long) request.getAttribute(LOGGER_SEND_TIME);


        //设置请求时间差
        accessLog.setDuration(Integer.valueOf((currentTime - sendTime)+""));

        accessLog.setCreateTime(new Date());
        //将sysLog对象持久化保存
        log.info(accessLog.toString());
        mongoTemplate.save(accessLog);
    }
}
