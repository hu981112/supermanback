package com.postsuperman.superback.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.postsuperman.superback.common.AccessLog;
import com.postsuperman.superback.common.ExceptionType;
import com.postsuperman.superback.common.GenericResponse;
import com.postsuperman.superback.common.JwtTokenUtil;

import com.postsuperman.superback.config.BufferedServletRequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class AuthFilter extends OncePerRequestFilter {

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private JwtTokenUtil util;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

//    @Autowired
//    private AccessLogMapper accessLogMapper;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String token = httpServletRequest.getHeader(util.getHeader());
        HttpServletRequest requestWrapper;
        String contentType = httpServletRequest.getContentType();
        String method = httpServletRequest.getMethod();
        if (HttpMethod.POST.matches(method) && org.apache.commons.lang3.StringUtils.isNotEmpty(contentType) && contentType.contains(MediaType.APPLICATION_JSON_VALUE)) {
            requestWrapper = new BufferedServletRequestWrapper(httpServletRequest);
        } else {
            requestWrapper = httpServletRequest;
        }
        if (antPathMatcher.match("/api/auth", httpServletRequest.getRequestURI())
                || antPathMatcher.match("/api/refresh", httpServletRequest.getRequestURI())
                || antPathMatcher.match("/api/users/register", httpServletRequest.getRequestURI())) {
            filterChain.doFilter(requestWrapper, httpServletResponse);
            return;
        }
        if (token != null && !token.isEmpty()) {
            String username = util.getUsernameFromToken(token);
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                if (!antPathMatcher.match("/api/auth", httpServletRequest.getRequestURI()) && !antPathMatcher.match("/api/refresh", httpServletRequest.getRequestURI()) && util.isTokenExpired(token)) {
//                    throw new GenericException(ExceptionType.TOKEN_EXPIRED, "Token 过期");
                    httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
                    httpServletResponse.setHeader("Content-Type", "application/json;charset=utf8");
                    ObjectMapper mapper = new ObjectMapper();
                    GenericResponse body = GenericResponse.error(ExceptionType.TOKEN_EXPIRED, "Token 过期");
                    AccessLog accessLog = AccessLog.getInstance(requestWrapper, httpServletResponse, body);
                    mongoTemplate.save(accessLog);
//                    log.info(accessLog.toString());
//                    accessLogMapper.insertAccessLog(accessLog);
                    httpServletResponse.getWriter().write(mapper.writeValueAsString(body));
                    return;
                }
                if (util.validateToken(token, userDetails)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        }
        filterChain.doFilter(requestWrapper, httpServletResponse);
    }
}
