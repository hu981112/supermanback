package com.postsuperman.superback.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageModel<T> {

    private List<T> datas;

    private Long rowCounts;

    private int pageSize;

    private int pageNo;



}
