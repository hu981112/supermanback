package com.postsuperman.superback.common;

import lombok.Data;

@Data
public class GenericResponse {

    private int status;
    private boolean isOk;
    private String message;
    private Object data;

    private GenericResponse() {}

    public static GenericResponse error(GenericException e) {
        GenericResponse response = new GenericResponse();
        response.setOk(false);
        response.setStatus(e.getCode());

        if (e.getCode() == ExceptionType.USER_INPUT_ERROR.getCode() || e.getCode() == ExceptionType.FORBIDDEN_ERROR.getCode() || e.getCode() == ExceptionType.TOKEN_EXPIRED.getCode()) {
            response.setMessage(e.getMessage());
        } else {
            response.setMessage(e.getMessage() + "，请重新尝试输入或联系管理员");
        }
        return response;
    }

    public static GenericResponse error(ExceptionType type, String message) {
        GenericResponse response = new GenericResponse();
        response.setOk(false);
        response.setStatus(type.getCode());
        response.setMessage(message);

        return response;
    }

    public static GenericResponse success() {
        GenericResponse response = new GenericResponse();
        response.setOk(true);
        response.setStatus(200);
        response.setMessage("success");

        return response;
    }

    public static GenericResponse success(Object data) {
        GenericResponse response = success();
        response.setData(data);
        return response;
    }

}
