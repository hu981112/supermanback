package com.postsuperman.superback.common;

//import com.alibaba.excel.annotation.ExcelProperty;
//import com.alibaba.excel.annotation.format.DateTimeFormat;
//import com.xjtuse.smartplatform.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * 对象操作工具类
 * @author 林子牛
 *
 */
@Slf4j
public class ObjectUtil {

    private final static String PACKAGE_NAME="com.xjtuse.rock.entity.data";
    /**
     * 根据传入的数据实体类名获取类的全限定名
     * @param className 类名
     * @return 全限定名
     */
    public static String getFullClassName(String className) {
        return PACKAGE_NAME + "." + className;
    }

    private ObjectUtil() {
    }

    public static boolean checkObject(Object o) {
        try {
            for (Field field : o.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (!"id".equals(field.getName())) {
                    Object obj = field.get(o);
                    if (obj instanceof CharSequence) {
                        if (ObjectUtils.isEmpty(obj)) {
                            return false;
                        }
                    } else {
                        if (null == obj) {
                            return false;
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * 判断类中每个属性是否都为空
     * @param  o 待判断的对象
     * @return 对象属性全部是否为空
     */
    public static boolean allFieldIsNULL(Object o) {
        try {
            // 遍历对象中的每一个字段
            for (Field field : o.getClass().getDeclaredFields()) {
                // 将字段设置为可访问
                field.setAccessible(true);
                // 获取该字段的值
                Object object = field.get(o);
                // 判断是否为字符串类型
                if (object instanceof CharSequence) {
                    // 判断字符串是否为空
                    if (!ObjectUtils.isEmpty(object)) {
                        return false;
                    }
                } else {
                    if (null != object) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            log.error("判断对象属性为空异常", e);
        }
        return true;
    }



    /**
     * 将视图层获取的 JSON 数据转换为对应的实体类对象
     * @param clas 对应实体类
     * @param data 视图层 JSON
     * @return 对应的对象
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws ParseException
     */
    public static Object getObject(Class clas, Map data) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ParseException {
        // clas 为希望生成实体类的类对象
        // data 为接收到的 Map 数据

        // 通过反射获取实体类中所有的域
        Field[] fields = clas.getDeclaredFields();

        // 使用反射，通过构造函数创建对象
        Object o = clas.getConstructor().newInstance();

        // 使用反射为实体对象赋值
        for (int i = 0; i < fields.length; i++) {
            Field f = fields[i];
            // 如果域为 private ，使用前需设置 Accessible 为 true，才可以赋值
            f.setAccessible(true);

            // 变量名在实体类中使用 @JsonProperty 注解标注
            // 这里通过反射获取 @JsonProperty 的值
            String varName = f.getAnnotation(JsonProperty.class).value();

//            // 判断该字段上有没有加上 @Nullable 注解
//            if (f.getAnnotation(Nullable.class) != null && !f.getAnnotation(Nullable.class).value()) {
//                // 如果没有，检查输入变量是否为空
//                if (data.get(varName) == null || StringUtils.isBlank(data.get(varName).toString())) {
//                    throw new NullPointerException("字段 " + varName + " 不能为空");
//                }
//            }

            // 获取字段的类型，不同类型不同的处理方式
            String type = f.getType().getSimpleName();
            if ("id".equals(varName)) {
                f.set(o, null);
                continue;
            }
            if ("Double".equals(type)) {
                // 如果字段为 Double 类型
                Double d;
                try {
                    Object temp = data.getOrDefault(varName, null);
                    if (temp == null) {
                        d = null;
                    } else {
                        d = Double.valueOf(temp.toString());
                    }
                } catch (NumberFormatException e) {
                    d = Double.NaN;
                }

                f.set(o, d);
            } else if ("Date".equals(type)) {
                // 如果字段为 Date 类型
                // 通过 @DateTimeFormat 获取其格式化方式
//                String format = f.getAnnotation(DateTimeFormat.class).value();
                // 使用上面获取的格式化方式来解析数据
                SimpleDateFormat sdf = new SimpleDateFormat();
                try {
                    f.set(o, sdf.parse(data.get(varName).toString()));
                } catch (Exception e) {
                    f.set(o, null);
                }

            }
            else if ("Integer".equals(type)) {
                // 字段为整数
                Integer num;
                try {
                    Object temp = data.getOrDefault(varName, null);
                    if (temp == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(temp.toString());
                    }
                } catch (NumberFormatException e) {
                    num = null;
                }
                f.set(o, num);
            } else if ("Boolean".equals(type)) {
                f.set(o, Boolean.valueOf(data.getOrDefault(varName, true).toString()));
            }
            else {
                // 剩下的情况
                if (data.get(varName) != null) {
                    f.set(o, data.get(varName).toString());
                } else {
                    f.set(o, null);
                }
            }
        }
        // 返回生成好的对象
        return o;

    }

    public static void copyData(Object source, Object dest) throws IllegalAccessException {
        // 获取源数据实体的类对象
        Class clasSource = source.getClass();
        // 获取目的实体的类对象
        Class clasDest = dest.getClass();

        // 获取它们所有的域
        Field[] fields = clasDest.getDeclaredFields();
        Field[] fieldSource = clasSource.getDeclaredFields();
        // 检测重复出现的域，并复制域的值
        for (Field f : fields) {
            f.setAccessible(true);
            BsonProperty property = f.getAnnotation(BsonProperty.class);
            if (property != null) {
                String varName = property.value();
                for (Field f1 : fieldSource) {
                    f1.setAccessible(true);
                    BsonProperty temp = f1.getAnnotation(BsonProperty.class);
                    if (temp != null) {
                        if (varName.equals(temp.value())) {
                            f.set(dest, f1.get(source));
                            break;
                        }
                    }
                }
            }
        }
    }

    //蛇形转驼峰形
    public static String toHumpString(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] str = string.split("_");
        int i = 0;
        for (String string2 : str) {
            if (i != 0) {
                stringBuilder.append(string2.substring(0, 1).toUpperCase());
                stringBuilder.append(string2.substring(1));
            } else {
                stringBuilder.append(string2);
            }
            i++;
        }
        return stringBuilder.toString();
    }

    public static String toHumpString2(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] str = string.split("_");
        for (String string2 : str) {
            stringBuilder.append(string2.substring(0, 1).toUpperCase());
            stringBuilder.append(string2.substring(1));
        }
        return stringBuilder.toString();
    }

    public static String toLineString(String string) {

        StringBuilder sb = new StringBuilder(string);

        int temp = 0;
        for(int i=0 ;i<string.length() ; i++){
            if(Character.isUpperCase(string.charAt(i))){
                sb.insert(i+temp,"_");
                temp+=1;
            }
        }
        return sb.toString().toLowerCase();

    }


}
