package com.postsuperman.superback.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "accesslog")
public class AccessLog {

    @Id
    private String id;

    private String username;

    private String url;

    private Integer duration;

    private String httpMethod;

    private int httpStatus;

    private String message;

    private String ip;

    private Date createTime;

    private String requestBody;

    private String queryString;

    public static AccessLog getInstance(HttpServletRequest request, HttpServletResponse response, GenericResponse body) throws IOException {
        AccessLog accessLog = new AccessLog();
        //accessLog.setIp(AddressIpUtils.getIpAdrress(request));
        accessLog.setHttpMethod(request.getMethod());
        accessLog.setUrl(request.getRequestURI());
        int status = response.getStatus();
        accessLog.setHttpStatus(status);
        String message = body.getMessage();
        accessLog.setMessage(message);
        String token = request.getHeader("auth");
        try {
            BufferedReader reader = request.getReader();
            String requestBody = IOUtils.toString(reader);
            if (requestBody.length() > 2048) {
                accessLog.setRequestBody(requestBody.substring(0, 2048));
            } else {
                accessLog.setRequestBody(requestBody);
            }

        } catch (Exception ignored) {
        }

        accessLog.setQueryString(request.getQueryString());
        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
        accessLog.setUsername(jwtTokenUtil.getUsernameFromToken(token));
        accessLog.setCreateTime(new Date());
        return accessLog;
    }

}
