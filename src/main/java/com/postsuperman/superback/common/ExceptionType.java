package com.postsuperman.superback.common;

public enum ExceptionType {
    USER_INPUT_ERROR(400, "用户输入错误"),
    TOKEN_EXPIRED(401, "Token 过期"),
    FORBIDDEN_ERROR(403, "无权访问"),
    NOT_FOUND(404, "Not Found"),
    SYSTEM_ERROR(500, "系统错误"),
    UNKNOWN_ERROR(999, "未知错误");

    int code;
    String desc;

    ExceptionType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
