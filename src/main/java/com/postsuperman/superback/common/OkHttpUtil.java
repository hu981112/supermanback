package com.postsuperman.superback.common;
//okhttp3请求工具类

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.postsuperman.superback.entity.BO.HttpParmBO;
import com.postsuperman.superback.entity.PO.HttpReqPO;
import okhttp3.*;
import org.springframework.http.ResponseEntity;

public class OkHttpUtil {

    //生成okhttp的默认请求头
    private static Map<String, String> defaultHeaderMap;

    static {
        defaultHeaderMap = new HashMap<>();
        defaultHeaderMap.put("Content-Type", "application/json");
        defaultHeaderMap.put("Accept", "*/*");
        defaultHeaderMap.put("Connection", "keep-alive");
        defaultHeaderMap.put("Accept-Language", "zh-CN,zh;q=0.9");
        defaultHeaderMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
    }


    private static Headers getHeaders(List<HttpParmBO> headerlist) {
        Map<String, String> headerMap = new HashMap<>(defaultHeaderMap);
        if (headerlist != null) {
            headerlist.forEach(httpParmBO -> {
                        headerMap.put(httpParmBO.getKey(), httpParmBO.getVal());
                    }
            );
        }
        Headers.Builder headerbuilder = new Headers.Builder();
        headerMap.forEach((key, value) -> {
            headerbuilder.add(key, value);
        });
        return headerbuilder.build();
    }

    public static Response get(HttpReqPO httpReqPO) throws IOException {
        String url = httpReqPO.getAimurl();
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();
        Headers headers = getHeaders(httpReqPO.getHeader());
        Request request = new Request.Builder()
                .headers(headers)
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        return response;
    }

    public static Response post(HttpReqPO httpReqPO) throws IOException {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();
        Headers headers = getHeaders(httpReqPO.getHeader());
        RequestBody requestBody = RequestBody.create(ConvertBody(httpReqPO), MediaType.parse(httpReqPO.getType()));
        Request request = new Request.Builder()
                .headers(headers)
                .url(httpReqPO.getAimurl())
                .post(requestBody)
                .build();
        return client.newCall(request).execute();
    }

    public static Response put(HttpReqPO httpReqPO) throws IOException {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();
        Headers headers = getHeaders(httpReqPO.getHeader());
        RequestBody requestBody = RequestBody.create(ConvertBody(httpReqPO), MediaType.parse(httpReqPO.getType()));
        Request request = new Request.Builder()
                .headers(headers)
                .url(httpReqPO.getAimurl())
                .put(requestBody)
                .build();
        return client.newCall(request).execute();
    }

    public static Response delete(HttpReqPO httpReqPO) throws IOException {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();
        Headers headers = getHeaders(httpReqPO.getHeader());
        RequestBody requestBody = RequestBody.create(ConvertBody(httpReqPO), MediaType.parse(httpReqPO.getType()));
        Request request = new Request.Builder()
                .headers(headers)
                .url(httpReqPO.getAimurl())
                .delete(requestBody)
                .build();
        return client.newCall(request).execute();
    }

    public static String ConvertBody(HttpReqPO httpReqPO) throws IOException {
        if (httpReqPO.getBody() != null) {
            JSONObject jsonObject = new JSONObject();
            if (httpReqPO.getBody() instanceof List) {
                List<HttpParmBO> list = (List<HttpParmBO>) httpReqPO.getBody();
                list.forEach(httpParmBO -> {
                    jsonObject.put(httpParmBO.getKey(), convertValue(httpParmBO));
                });
                return jsonObject.toJSONString();
            } else
                return JSON.toJSONString(httpReqPO.getBody());
        }
        return null;
    }

    public static Object convertValue(HttpParmBO httpParmBO) {
        {
            switch (httpParmBO.getType()) {
                case "int":
                    return Integer.parseInt(httpParmBO.getVal());
                case "long":
                    return Long.parseLong(httpParmBO.getVal());
                case "double":
                    return Double.parseDouble(httpParmBO.getVal());
                case "float":
                    return Float.parseFloat(httpParmBO.getVal());
                case "boolean":
                    return Boolean.parseBoolean(httpParmBO.getVal());
                case "byte":
                    return Byte.parseByte(httpParmBO.getVal());
                case "short":
                    return Short.parseShort(httpParmBO.getVal());
                case "char":
                    return httpParmBO.getVal().charAt(0);
                default:
                    return httpParmBO.getVal();
            }
        }
    }
}

