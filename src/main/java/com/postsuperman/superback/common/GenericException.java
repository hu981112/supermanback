package com.postsuperman.superback.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class GenericException extends RuntimeException {

    private int code;
    private String message;
    @JsonIgnore
    private String stacktrace;

    public GenericException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public GenericException(ExceptionType type, String message) {
        this.code = type.getCode();
        this.message = message;
    }

    public GenericException(ExceptionType type, String message, String stacktrace) {
        this.code = type.getCode();
        this.message = message;
        this.stacktrace = stacktrace;
    }

    public GenericException(ExceptionType type) {
        this.code = type.getCode();
        this.message = type.getDesc();
    }


    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getStacktrace() {
        return this.stacktrace;
    }

}
