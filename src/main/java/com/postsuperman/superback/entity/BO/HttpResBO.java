package com.postsuperman.superback.entity.BO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpResBO {
    String url;

    String header;

    String method;

    String status;

    String body;

    String time;

    String contentType;
}