package com.postsuperman.superback.entity.BO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpParmBO {
    String key;
    String val;
    Boolean require;
    String type;
    String doc;
    Boolean enable;
}
