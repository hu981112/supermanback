package com.postsuperman.superback.entity.BO;


import com.postsuperman.superback.entity.PO.HttpReqPO;
import com.postsuperman.superback.entity.PO.HttpResPO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpTaskBO {
    String id;

    String taskname;

    HttpReqBO request;

    HttpResBO response;

}