package com.postsuperman.superback.entity.BO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpReqBO {

    String url;

    List<HttpParmBO> header;

    String method;

    List<HttpParmBO> query;

    Object body;

    String type;

}
