package com.postsuperman.superback.entity.PO;

import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpResBO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "httptask")
public class HttpTaskPO {
    @Id
    String id;

    String userid;

    String taskname;

    Date time;

    HttpReqBO request;

    HttpResBO response;
}
