package com.postsuperman.superback.entity.PO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpResPO {
    String url;
    String header;
    String method;
    String status;
    String contentType;
    String body;
    String time;
}