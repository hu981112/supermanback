package com.postsuperman.superback.entity.PO;

import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpResBO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "httpshare")
public class HttpSharePO  {
    @Id
    String id;

    String userId;

    String shareName;

    String uuid;

    List<String> taskIds;

    Date createTime;

}
