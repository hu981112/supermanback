package com.postsuperman.superback.entity.PO;

import com.postsuperman.superback.entity.BO.HttpParmBO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpReqPO {
    String userid;

    String srcurl;

    String aimurl;

    String method;

    List<HttpParmBO> header;

    List<HttpParmBO> query;

    Object body;

    String type;

}
