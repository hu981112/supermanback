package com.postsuperman.superback.entity.PO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "batchtest")
public class BatchTestPO {
    @Id
    String id;

    String userid;

    String testName;

    List<HttpTaskPO> tasks;

    Date createTime;


}
