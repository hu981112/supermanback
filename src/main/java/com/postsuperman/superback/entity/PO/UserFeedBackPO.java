package com.postsuperman.superback.entity.PO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "userfeedback")
public class UserFeedBackPO {
    @Id
    String id;

    String userid;

    String type;

    String text;

    Date createTime;
}
