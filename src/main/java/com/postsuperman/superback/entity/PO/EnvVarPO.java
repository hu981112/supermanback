package com.postsuperman.superback.entity.PO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "envvar")
public class EnvVarPO {
    @Id
    String id;
    String userid;
    String name;
    String value;
}
