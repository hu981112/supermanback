package com.postsuperman.superback.entity.PO;

import com.postsuperman.superback.common.GenericException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionLogPO {

    private Long id;

    private Integer code;

    private String exceptionName;

    private String message;

    private String stacktrace;

    private Date time;

    public static ExceptionLogPO getInstance(Exception e) {
        ExceptionLogPO result;
        if (e instanceof GenericException) {
            GenericException genericException = (GenericException) e;
            result = ExceptionLogPO.builder()
                    .code(genericException.getCode())
                    .exceptionName(genericException.getClass().getName())
                    .message(genericException.getMessage())
                    .stacktrace(ExceptionUtils.getStackTrace(e))
                    .build();
        } else {
            result = ExceptionLogPO.builder()
                    .code(1)
                    .exceptionName(e.getClass().getName())
                    .message(e.getMessage())
                    .stacktrace(ExceptionUtils.getStackTrace(e))
                    .build();
        }
        result.setTime(new Date());
        if (StringUtils.isEmpty(result.getMessage())) {
            result.setMessage("Null Pointer");
        }
        return result;
    }

}
