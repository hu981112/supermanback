package com.postsuperman.superback.entity.PO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * 测试mongodb
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("User")
public class UserPO {

    @Id
    private String id;

    private String username;

    private String password;

    private String phone;

    private Boolean enabled;

    List<String>Roles;

    private Date createTime;

    private static final long serialVersionUID = 1L;


}