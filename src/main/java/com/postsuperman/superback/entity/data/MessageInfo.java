package com.postsuperman.superback.entity.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageInfo implements Serializable {
    private String sender;

    private String receiver;

    private String message;
}
