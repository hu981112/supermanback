package com.postsuperman.superback.entity.VO;

import com.postsuperman.superback.entity.PO.HttpTaskPO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpTaskListVO {

    String url;
    List<HttpTaskPO> cases;

}
