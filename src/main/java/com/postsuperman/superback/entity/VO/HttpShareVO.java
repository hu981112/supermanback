package com.postsuperman.superback.entity.VO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpShareVO {
    String id;

    String userId;

    String shareName;

    List<HttpTaskVO> tasks;

    Date createTime;
}
