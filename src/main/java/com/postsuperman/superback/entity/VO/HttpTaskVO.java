package com.postsuperman.superback.entity.VO;

import com.postsuperman.superback.entity.BO.HttpReqBO;
import com.postsuperman.superback.entity.BO.HttpResBO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Date;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HttpTaskVO {
    String id;

    String taskname;

    String url;

    Date time;
}
