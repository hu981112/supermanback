package com.postsuperman.superback.config;

import com.postsuperman.superback.filter.AccessLogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationWebConfig implements WebMvcConfigurer {
    private final AccessLogInterceptor accessLogInterceptor;

    @Autowired
    public ApplicationWebConfig(AccessLogInterceptor accessLogInterceptor) {
        this.accessLogInterceptor = accessLogInterceptor;
    }
    private final String[] excludePath = {
            "/static",
            "/test/**",
    };

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(accessLogInterceptor).addPathPatterns("/**").excludePathPatterns(excludePath);
    }
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("*")
                .allowedHeaders("*")
                .allowCredentials(true)
                .exposedHeaders(
                        "Content-Type",
                        "X-Requested-With",
                        "accept",
                        "auth",
                        "Origin",
                        "Access-Control-Request-Method",
                        "Access-Control-Request-Headers"
                );

    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//            registry.addResourceHandler("/static/**").addResourceLocations(wellloc);
//    }

}
